### Firmware nRF24L01+ para Raspberry Pi 1B e 2B, Arduino e MSP430 ###

Aqui você encontra a biblioteca do módulo nRF24L01+ para Raspberry Pi 1 e 2, Arduino, MSP430 e muitas outras;

Placas suportadas:  
  
* Uno, Nano, etc (placas baseadas no Atmega328)  
* Mega (2560, 1280, etc)  
* ARM (Arduino Due) via métodos SPI extendidos
* ATTiny 24/44/84 25/45/85  
* Raspberry Pi
* Raspberry Pi 2
* TI MSP430 Launchpad
* Esta biblioteca é um fork de outras, que foi ampliada para suportar mais placas.
* [Assista aqui](https://youtu.be/azbWU3yyYkU) a biblioteca funcionando.

### Como utilizar? ###

* Dentro da pasta RF24_library tem as bibliotecas e as instruções de instalação para cada plataforma.

### Com quem falo para obter ajuda? ###

* Em caso de dúvidas, críticas ou sugestões, envie e-mail para marco.rabelo@gmail.com
* [Inscreva-se no meu canal](https://www.youtube.com/channel/UC05P95nXawYc15gIpN8GFPw?sub_confirmation=1) no youtube para receber as novidades.