#include "Enrf24.h"
#include "nRF24L01.h"
#include <string.h>
#include <SPI.h>

boolean DEBUG_MODE = true;

Enrf24 radio(P2_0, P2_1, P2_2);  // P2.0=CE, P2.1=CSN, P2.2=IRQ
const uint8_t txaddr[] = { 0xE7, 0xD3, 0xF0, 0x35, 0xFF }; //Este e o endereco para onde estou transmitindo
const uint8_t rxaddr[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0x02 }; //Este e o endereco de recepcao

#define LED_R P1_0
#define LED_G P1_3
#define LED_B P1_4

const int max_payload_size = 16; // Default e 32
unsigned char receive_payload[max_payload_size + 1];
const unsigned int delay_no_debug = 100; // Tempo de delay no caso de nao estar no modo debug

void setup()
{
    pinMode(LED_R, OUTPUT);
    pinMode(LED_G, OUTPUT);
    pinMode(LED_B, OUTPUT);
    digitalWrite(LED_R, HIGH);
    digitalWrite(LED_G, HIGH);
    digitalWrite(LED_B, HIGH);

    if(DEBUG_MODE)
    {
      Serial.begin(9600);
    }

    SPI.begin();
    SPI.setDataMode(SPI_MODE0);
    SPI.setBitOrder(MSBFIRST);

    //radio.begin( 1000000, 76 );  // Padrao 1Mbps, canal 0
    radio.begin(250000, 76);
    radio.setCRC(1, 1);

    radio.setTXaddress((void*)txaddr);
    radio.setRXaddress((void*)rxaddr);
    
    radio.enableRX();
}

void loop()
{
    // Se houver dados disponiveis
    if (radio.available())
    {
        // Obter as cargas úteis até que tenhamos conseguido tudo
        uint8_t len;
        
        while (radio.available())
        {
            // Buscar a carga, e ver se este foi o último.
            len = radio.read(receive_payload);
            
            // Imprime o o que foi recebido
            if(DEBUG_MODE)
            {
                Serial.print("Informacao recebida: ");
                Serial.println((char*)receive_payload);
            }
            
            // Coloca um 0 no final para facilitar a impressao.
            receive_payload[len] = 0;
            
            if(!DEBUG_MODE)
            {
                delay(delay_no_debug);
            }
        }
        
        // Para a escuta para poder transmitir.
        radio.disableRX();
       
        int comando = (int)receive_payload[0];
        
        if(comando == 48) // Caracter 0 na tabela ascii
        {
            digitalWrite(LED_R, LOW);
            radio.write("LED Vermelho ON");
        }
        else if(comando == 49) // Caracter 1 na tabela ascii
        {
            digitalWrite(LED_R, HIGH);
            radio.write("LED Vermelho OFF");
        }
        else if(comando == 50) // Caracter 2 na tabela ascii
        {
            digitalWrite(LED_G, LOW);
            radio.write("LED Verde ON");
        }
        else if(comando == 51) // Caracter 3 na tabela ascii
        {
            digitalWrite(LED_G, HIGH);
            radio.write("LED Verde OFF");
        }
        else if(comando == 52) // Caracter 4 na tabela ascii
        {
            digitalWrite(LED_B, LOW);
            radio.write("LED Azul ON");
        }
        else if(comando == 53) // Caracter 5 na tabela ascii
        {
            digitalWrite(LED_B, HIGH);
            radio.write("LED Azul OFF");
        }
        else
        {
            radio.write("Comando invalido");
        }
        
        radio.flush();
        
        if(DEBUG_MODE)
        {
            Serial.println("Resposta enviada.");
        }
        
        // Voltando a escutar para poder capturar novos pacotes.
        radio.enableRX();
    }
}
