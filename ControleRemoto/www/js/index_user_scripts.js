/*jshint browser:true */
/*global $ */(function()
{
 "use strict";
 /*
   hook up event handlers 
 */
 function register_event_handlers()
 {     
    $("#red_led").click(function () {
        var state = this.checked ? "on_r" : "off_r";
        //$.ajaxSetup({ cache: true});
        $.getJSON("http://192.168.1.5/send?state=" + state, function(data){
            $("#txtMensagem").val(data.cmd_out);
        });
    });

    $("#green_led").click(function () {
        var state = this.checked ? "on_g" : "off_g";
        $.getJSON("http://192.168.1.5/send?state=" + state, function(data){
            $("#txtMensagem").val(data.cmd_out);
        });
    });

    $("#blue_led").click(function () {
        var state = this.checked ? "on_b" : "off_b";
        $.getJSON("http://192.168.1.5/send?state=" + state, function(data){
            $("#txtMensagem").val(data.cmd_out);
        });
    });
}
 document.addEventListener("app.Ready", register_event_handlers, false);
})();
